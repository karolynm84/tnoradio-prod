import { Component, OnInit } from '@angular/core';
import { MenuOpenedService } from '../menu-opened.service';
import { DomSanitizer  } from '@angular/platform-browser';


@Component({
  selector: 'app-page-container',
  templateUrl: './page-container.component.html',
  styleUrls: ['./page-container.component.css', '../app.component.css']
})
export class PageContainerComponent implements OnInit {
  _isOpened: boolean;
  innerWidth: any;
  style: string;

  constructor(private data: MenuOpenedService, private sanitizer: DomSanitizer) { }
 

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.data._isOpened.subscribe(menuIsOpened => {      
      this._isOpened = menuIsOpened;
      console.log("OPENED? " + menuIsOpened);
    });
  }

  getSideBarStyle() {
    if(this._isOpened) {
      if (this.innerWidth > 1200) {
        this.style = 'margin-left:-10px; transition: 0.1s; width:200%;';
      }
      else {
        this.style = 'margin-left:-10px; transition: 0.1s; width:200%;';
      }
      return this.sanitizer.bypassSecurityTrustStyle(this.style);
     
    } else {
      this.style = '';
      this.sanitizer.bypassSecurityTrustStyle(this.style);
     
    }
  }

  getCol11Style() {
    if(this._isOpened) {
      if (this.innerWidth > 1200) {
        this.style = ' margin-left:-7%; transition: 0.1s; ';
      }
      else {
        this.style = ' margin-left:-4%; transition: 0.1s; ';
      }
      return this.sanitizer.bypassSecurityTrustStyle(this.style);
     
    } 
  }
}
