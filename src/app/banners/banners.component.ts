import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImageSize, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
    selector:"app-banners",
    templateUrl: './banners.component.html',
    styleUrls: ['./banners.component.css'],
})
export class BannersComponent implements OnInit {
    innerWidth: number;
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];

    ngOnInit(): void {

       this.innerWidth = window.innerWidth;

       this.galleryOptions = [
        {
            width: '1300px',
            height: '235px',
            imageAutoPlay: true,
            imageAutoPlayPauseOnHover: true,
            imageSize: NgxGalleryImageSize.Cover,
            imageAnimation: NgxGalleryAnimation.Slide,
            imageAutoPlayInterval: 4000,
            thumbnails: false,
            preview: false
        },
        {
            breakpoint: 1366,
            height: '185px',
        },
        {
            breakpoint: 1200,
            height: '165px',
        },
        {
            breakpoint: 1100,
            height: '155px',
        },
        {
            breakpoint: 1024,
            height: '140px',
            width: '980px',
            imageArrowsAutoHide:true
        },
        // max-width 
        {
            breakpoint: 768,
            width: '768px',
            height: '120px',
            imageArrowsAutoHide:true,
        },
         // max-width 
         {
            breakpoint: 600,
            width: '600px',
            height: '350px',
            imageArrowsAutoHide:true,
        },
        {
            breakpoint: 414,
            width: '414px',
            height: '250px',
            imageArrowsAutoHide:true
        },
        {
            breakpoint: 375,
            width: '375px',
            height: '230px',
            imageArrowsAutoHide:true
        },       
        {
            breakpoint: 360,
            width: '360px',
            height: '220px',
            imageArrowsAutoHide:true
        },
        {
            breakpoint: 320,
            width: '320px',
            height: '200px',
            imageArrowsAutoHide:true
        }
    ];


       if (this.innerWidth <= 600) {
            this.galleryImages = [
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_culturizate-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_culturizate-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_culturizate-small.jpg'
                },            
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_deportes-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_deportes-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_deportes-small.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_aprendeaemprender-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_aprendeaemprender-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_aprendeaemprender-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_somosdecadas-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_somosdecadas-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_somosdecadas-small.jpg'
                },   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_zonavip-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_zonavip-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_zonavip-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg'
                },             
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_forycontigo-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_forycontigo-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_forycontigo-small.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg'
                },   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg'
                }            
            ];
       }
        else {

            this.galleryImages = [
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_piensaengrande.jpg',
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_bebavandenberg.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_genteextraordinaria.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_culturizate-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_culturizate.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_culturizate.jpg'
                },            
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_deportes-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_deportes.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_deportes.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entrevistastonycarrasco.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_franquicias-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_franquicias.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_franquicias.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_aprendeaemprender-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_aprendeaemprender.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_aprendeaemprender.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_somosdecadas-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_somosdecadas.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_somosdecadas.jpg'
                },   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_vermasalla.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_buenasvibras.jpg'
                },          
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_entretodos-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_entretodos.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_entretodos.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_telonvip-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_telonvip.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_telonvip.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_metaforasanadoras.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_zonavip-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_zonavip.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_zonavip.jpg'
                },  
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_hostseven-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_hostseven.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_hostseven.jpg'
                },             
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_snobgourmet.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_nocheradiante.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_forycontigo-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_forycontigo.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_forycontigo.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_driblingyrebote.jpg'
                },    
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_emprendeycomprende.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_emprendeycomprende.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_emprendeycomprende.jpg'
                },   
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_dequepuedes.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_mujerfeliz.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_mujerfeliz.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_mujerfeliz.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_tumomentofitness.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_frecuenciasaludable.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_frecuenciasaludable.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_frecuenciasaludable.jpg'
                },    
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_historiasqueinspiran.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_alsondelosbailadores.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_alsondelosbailadores.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_alsondelosbailadores.jpg'
                }, 
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta-small.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_horaperfecta.jpg'
                },
                {
                    small: 'assets/images/banners/radio_online_venezuela_fondo_suenatuensamble.jpg',
                    medium: 'assets/images/banners/radio_online_venezuela_fondo_suenatuensamble.jpg',
                    big: 'assets/images/banners/radio_online_venezuela_fondo_suenatuensamble.jpg'
                },              
            ];
       }
    }
}