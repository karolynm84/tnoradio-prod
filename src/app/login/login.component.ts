import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { loginForm } from './login.form';
//import { UserService } from '../user/user.service';

import { Logger } from 'angular2-logger/core';
import { Login } from './login.model';
import { FormsValidationService } from '../shared-components/forms-validation.service';
//import { User } from '../user/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [FormsValidationService]
})

export class LoginComponent implements OnInit {    
    form1: FormGroup;
    userLogin: Login;
    //user: User;
    email: string;     
    sub: any;   

  constructor (
    public router:Router,
    public formBuilder: FormBuilder,
    public validation: FormsValidationService,
    //public userService: UserService,
    public route: ActivatedRoute,
    public toastr: ToastrService,
    public vcr: ViewContainerRef,
    public logger: Logger ) {}

  ngOnInit() {
    //this.toastr.setRootViewContainerRef(this.vcr);   
    this.form1 = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required, this.validation.emailValidator, Validators.maxLength(40)])],
        password:  ['', Validators.compose([Validators.required,  Validators.minLength(6),  Validators.maxLength(16)])]
    });  
  }

  /**
   * When user press submit button.
   */
  onSubmit({ value, valid }: { value: loginForm, valid: boolean }) { 
    this.logger.info(valid);   
    /*this.sub = this.route.params.subscribe(params => {
        const user = params['email'];
                
        this.userService
        .getUser(this.form1.get('email').value)
        .subscribe(c => {
          this.user = c;
          console.log("ESTO es C" + c);
        },error=>{
          this.toastr.info('Something went wrong'); 
        }
      ); 
      },error=>{
        //this.toastr.info('Something went wrong'); 
      }
    );  */        
  }
}
