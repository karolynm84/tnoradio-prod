import { TestBed, inject } from '@angular/core/testing';

import { FormsValidationService } from './forms-validation.service';

describe('FormsValidationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormsValidationService]
    });
  });

  it('should be created', inject([FormsValidationService], (service: FormsValidationService) => {
    expect(service).toBeTruthy();
  }));
});
