import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { PageContainerComponent } from './page-container/page-container.component';
import { LoginComponent } from './login/login.component';
import { ProgramacionComponent } from './programacion/programacion.component';

export const routes: Routes = [
  {
    path: '',
    component: PageContainerComponent
  },
  {
    path: 'home',
    component: PageContainerComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'programacion',
    component: ProgramacionComponent
  },
  {
    path: '**',
    component: PageContainerComponent
  }
];

@NgModule({
  imports: [
    CommonModule,  RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})

export class AppRoutingModule { }