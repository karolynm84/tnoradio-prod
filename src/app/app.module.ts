import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { NgModule } from '@angular/core';

import { AlertModule } from 'ngx-bootstrap';

import { SidebarModule } from 'ng-sidebar';

import { CollapseModule } from 'ngx-bootstrap/collapse';

import { NgxGalleryModule } from 'ngx-gallery';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Logger, Options, Level } from "angular2-logger/core";

import 'hammerjs';

import { AppComponent } from './app.component';
import { TopHeaderComponent } from './top-header/top-header.component';
import { PubliHeaderComponent } from './publi-header/publi-header.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { PlayerComponent } from './player/player.component';
import { PlayerSideComponent } from './player-side/player-side.component';
import { PageContainerComponent } from './page-container/page-container.component';
import { MenuOpenedService } from 'src/app/menu-opened.service';
import { RedesComponent } from './redes/redes.component';
import { BannersComponent } from './banners/banners.component';
import { FooterComponent } from './footer/footer.component';
import { GestionProgramasComponent } from './gestion-programas/gestion-programas.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import { ProgramacionComponent } from './programacion/programacion.component';

@NgModule({
  declarations: [
    AppComponent,
    TopHeaderComponent,
    PubliHeaderComponent,
    SideBarComponent,
    PlayerComponent,
    PlayerSideComponent,
    PageContainerComponent,
    RedesComponent,
    BannersComponent,
    FooterComponent,
    GestionProgramasComponent,
    LoginComponent,
    ProgramacionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxGalleryModule,
    AlertModule.forRoot(),
    SidebarModule.forRoot(),
    CollapseModule.forRoot(),
    ToastrModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [MenuOpenedService, Logger],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private logger: Logger) {
  //this.logger.level = environment.logger;
 }
}
