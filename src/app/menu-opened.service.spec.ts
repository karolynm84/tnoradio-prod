import { TestBed, inject } from '@angular/core/testing';

import { MenuOpenedService } from './menu-opened.service';

describe('MenuOpenedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenuOpenedService]
    });
  });

  it('should be created', inject([MenuOpenedService], (service: MenuOpenedService) => {
    expect(service).toBeTruthy();
  }));
});
