import { Component, OnInit } from '@angular/core';
import { MenuOpenedService } from '../menu-opened.service';
import { DomSanitizer  } from '@angular/platform-browser';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss','../app.component.css']
})

export class SideBarComponent implements OnInit {

  _opened: boolean = false;
 
  _toggleSidebar() {
    this._opened = !this._opened;
    this.data.openClose(this._opened);
    console.log("SIDE BAR OPENED " + this._opened);
  }

  constructor(private data: MenuOpenedService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.data._isOpened.subscribe(menuIsOpened => this._opened = menuIsOpened);
    console.log('---SideBAr OnInit ---- Opened --' + this._opened);
  }

  getHamburguerStyle() {
    if(this._opened) {
      console.log("SIDE BURGER IS OPENED");
      const style = '	margin-left: 155% !important; transition: 0.1s;';
      return this.sanitizer.bypassSecurityTrustStyle(style);
     
    } else {
      const style = 'background-color:green';
      this.sanitizer.bypassSecurityTrustStyle(style);     
    }
  }
}
