import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MenuOpenedService {

  constructor() { }

  private _opened = new BehaviorSubject<boolean>(false);
  _isOpened = this._opened.asObservable();

  openClose(menuOpenedStatus: boolean) {
    console.log("Service menu opened is: " + menuOpenedStatus);
    this._opened.next(menuOpenedStatus);
  }
}

